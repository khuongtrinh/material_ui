import {
  Container,
  Grid,
  ImageList,
  ImageListItem,
  Link,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(5),
    position: "sticky",
    top: "0px",
  },
  title: {
    fontSize: 16,
    fontWeight: 500,
    color: "#555",
  },
  link: {
    marginRight: theme.spacing(2),
    color: "#555",
    fontSize: 16,
  },
  sidebarAboutBox: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[200],
  },
}));

function RightBar({ imageData, social }) {
  const classes = useStyles();
  return (
    <Container className={classes.container}>
      <Paper elevation={0} className={classes.sidebarAboutBox}>
        <Typography variant="h6" className={classes.title} gutterBottom>
          About
        </Typography>
        <Typography>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae,
          molestiae delectus! Consequuntur ut sed dolor cupiditate earum ex,
          quibusdam eius veniam. Animi assumenda atque totam ut voluptatem
          repellat unde exercitationem!
        </Typography>
      </Paper>
      <Typography variant="h6" className={classes.title} gutterBottom>
        Gallery
      </Typography>
      <ImageList rowHeight={160} className={classes.imageList} cols={3}>
        {imageData.map((item) => (
          <ImageListItem key={item.url} cols={item.cols || 1}>
            <img src={item.url} alt={item.title} />
          </ImageListItem>
        ))}
      </ImageList>
      <Typography variant="h6" gutterBottom className={classes.title}>
        Social
      </Typography>
      {social.map((network) => (
        <Link display="block" variant="body1" href="#" key={network.name}>
          <Grid container direction="row" spacing={1} alignItems="center">
            <Grid item>
              <network.icon />
            </Grid>
            <Grid item>{network.name}</Grid>
          </Grid>
        </Link>
      ))}
    </Container>
  );
}

export default RightBar;
