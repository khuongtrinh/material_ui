import { Container, makeStyles } from "@material-ui/core";
import Post from "./Post";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(5),
    padding: 0,
  },
}));

function Feed({ posts }) {
  const classes = useStyles();
  return (
    <Container className={classes.container}>
      {posts.map((post) => (
        <Post post={post} key={post.name} />
      ))}
    </Container>
  );
}

export default Feed;
