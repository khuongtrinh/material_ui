import {
  Avatar,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  IconButton,
  Link,
  makeStyles,
  Modal,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { LockOutlined, Search } from "@material-ui/icons";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: "space-between",
    overflowX: "auto",
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
  loginBut: {
    marginRight: "5px",
  },
  paper: {
    width: 500,
    height: 450,
    backgroundColor: "white",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: "auto",
    padding: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      width: "100vw",
      height: "100vh",
    },
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function NavBar({ sections }) {
  const [open, setOpen] = useState(false);
  const [openSignUp, setOpenSignUp] = useState(false);
  const classes = useStyles();
  return (
    <>
      <Toolbar className={classes.toolbar}>
        <Button size="small">Subscribe</Button>
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
          noWrap
          className={classes.toolbarTitle}
        >
          Anime Blog
        </Typography>
        <IconButton>
          <Search />
        </IconButton>
        <Button
          variant="outlined"
          size="small"
          className={classes.loginBut}
          onClick={() => {
            setOpen(true);
          }}
        >
          Sign in
        </Button>
        <Button
          variant="outlined"
          size="small"
          onClick={() => {
            setOpenSignUp(true);
          }}
        >
          Sign up
        </Button>
      </Toolbar>
      <Toolbar
        component="nav"
        variant="dense"
        className={classes.toolbarSecondary}
      >
        {sections.map((section) => (
          <Link
            color="inherit"
            noWrap
            key={section.title}
            variant="body2"
            href={section.url}
            className={classes.toolbarLink}
          >
            {section.title}
          </Link>
        ))}
      </Toolbar>
      <Modal
        open={open}
        onClose={() => {
          setOpen(false);
        }}
      >
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign In
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              style={{ width: "100%" }}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              style={{ width: "100%" }}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
              style={{ width: "100%" }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Modal>
      <Modal
        open={openSignUp}
        onClose={() => {
          setOpenSignUp(false);
        }}
      >
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign Up
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              style={{ width: "100%" }}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              name="password"
              label="Password"
              type="password"
              style={{ width: "100%" }}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              name="passwordAgain"
              label="Password Again"
              type="password"
              style={{ width: "100%" }}
            />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
              style={{ width: "100%" }}
            >
              Sign Up
            </Button>
            <Grid container>
              <Grid item>
                <Link href="#" variant="body2">
                  {"You have an account? Sign In"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Modal>
    </>
  );
}

export default NavBar;
