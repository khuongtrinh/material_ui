import { Container, Grid } from "@material-ui/core";
import Add from "./components/Add";
import Feed from "./components/Feed";
import Footer from "./components/Footer";
import MainFeaturedPost from "./components/MainFeaturedPost";
import NavBar from "./components/NavBar";
import RightBar from "./components/RightBar";
import GitHubIcon from "@material-ui/icons/GitHub";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";

const sections = [
  { title: "Adventure", url: "#" },
  { title: "Fantasy", url: "#" },
  { title: "Horror", url: "#" },
  { title: "Sports", url: "#" },
  { title: "Harem", url: "#" },
  { title: "School", url: "#" },
  { title: "Detective", url: "#" },
  { title: "Vampire", url: "#" },
  { title: "Comedy", url: "#" },
  { title: "Romance", url: "#" },
];

const mainFeaturedPost = {
  title: "Title of a longer featured blog post",
  description:
    "Multiple lines of text that form the lede, informing new readers quickly and efficiently about what's most interesting in this post's contents.",
  image:
    "https://wallpapersqueen.com/wp-content/uploads/2021/05/4k-Akatsuki-Wallpaper.jpg",
  imgText: "main image description",
  linkText: "Continue reading…",
};

const social = [
  { name: "GitHub", icon: GitHubIcon },
  { name: "Twitter", icon: TwitterIcon },
  { name: "Facebook", icon: FacebookIcon },
];

const imageData = [
  {
    url: "https://c4.wallpaperflare.com/wallpaper/200/379/762/kekkai-sensen-anime-anime-boys-blue-wallpaper-preview.jpg",
    cols: 2,
  },
  {
    url: "https://c4.wallpaperflare.com/wallpaper/226/867/800/kimetsu-no-yaiba-kamado-tanjir%C5%8D-anime-boys-hd-wallpaper-preview.jpg",
  },
  {
    url: "https://c4.wallpaperflare.com/wallpaper/884/100/6/anime-anime-boys-jujutsu-kaisen-satoru-gojo-hd-wallpaper-preview.jpg",
  },
  {
    url: "https://c4.wallpaperflare.com/wallpaper/591/238/424/naruto-shippuuden-uzumaki-naruto-uchiha-sasuke-rinnegan-wallpaper-preview.jpg",
  },
  {
    url: "https://c4.wallpaperflare.com/wallpaper/845/469/115/bleach-hollow-ichigo-ichigo-kurosaki-zangetsu-bleach-wallpaper-preview.jpg",
  },
  {
    url: "https://c4.wallpaperflare.com/wallpaper/212/1014/550/katekyo-hitman-reborn-anime-anime-boys-wallpaper-preview.jpg",
  },
  {
    url: "https://c4.wallpaperflare.com/wallpaper/283/291/754/one-piece-portgas-d-ace-hd-wallpaper-preview.jpg",
    cols: 2,
  },
];

const postData = [
  {
    name: "Hisoka",
    image: "https://images8.alphacoders.com/383/383559.png",
    desc: "Hisoka Morow (ヒソカ゠モロウ, Hisoka Morou) is a Hunter and former member #4 of the Phantom Troupe; his physical strength ranked third in the group. He is always in search for strong opponents, and would spare those who have great potential, such as Gon and Killua in order for them to get strong enough to actually challenge him. He originally served as the primary antagonist of the Hunter Exam arc and the Heavens Arena arc, before becoming a supporting character during the Yorknew City arc and Greed Island arc. During the 13th Hunter Chairman Election arc, he reprises his role as a secondary antagonist.",
  },
  {
    name: "Uchiha Itachi",
    image: "https://images2.alphacoders.com/106/106762.jpg",
    desc: "Itachi Uchiha (うちはイタチ, Uchiha Itachi) was a shinobi of Konohagakure's Uchiha clan who served as an Anbu Captain. He later became an international criminal after murdering his entire clan, sparing only his younger brother, Sasuke. He afterwards joined the international criminal organisation known as Akatsuki, whose activity brought him into frequent conflict with Konoha and its ninja — including Sasuke who sought to avenge their clan by killing Itachi. Following his death, Itachi's motives were revealed to be more complicated than they seemed and that his actions were only ever in the interest of his brother and village, making him remain a loyal shinobi of Konohagakure to the very end.",
  },
  {
    name: "Orochimaru",
    image: "https://images4.alphacoders.com/106/1061013.jpg",
    desc: "Orochimaru (大蛇丸, Orochimaru) is one of Konohagakure's legendary Sannin. With a life-ambition to learn all of the world's secrets, Orochimaru seeks immortality so that he might live all of the lives necessary to accomplish his task. After being caught red-handed performing unethical experiments on his fellow citizens for the sake of this immortality, Orochimaru defected from Konoha rather than be persecuted for his ambitions, and for many years sought the village's destruction in order to take revenge and demonstrate what he had learned. After several apparent deaths in the pursuit of his goals, Orochimaru realises his approach is flawed approach through Kabuto Yakushi, his former associate, and begins monitoring the choices and actions of his former apprentice, Sasuke Uchiha.",
  },
  {
    name: "Sosuke Aizen ",
    image:
      "https://images6.fanpop.com/image/photos/36600000/Aizen-image-aizen-36678332-2400-1350.jpg",
    desc: "Sōsuke Aizen (藍染 惣右介, Aizen Sōsuke) is the former captain of the 5th Division in the Gotei 13. He later leaves Soul Society with his followers, Gin Ichimaru and Kaname Tōsen. His lieutenant was Momo Hinamori. He formerly served as the lieutenant of the 5th Division under Shinji Hirako. After waging war against Soul Society with an army of Arrancar, Aizen was defeated by Ichigo Kurosaki and sealed away by Kisuke Urahara, and then imprisoned for his crimes.",
  },
];

function App() {
  return (
    <Container maxWidth="lg">
      <NavBar sections={sections} />
      <MainFeaturedPost post={mainFeaturedPost} />
      <Grid container>
        <Grid item xs={12} sm={8}>
          <Feed posts={postData} />
        </Grid>
        <Grid item sm={4} xs={12}>
          <RightBar imageData={imageData} social={social} />
        </Grid>
      </Grid>
      <Footer title="Anime Blog" description="Wibu neva die!" />
      <Add />
    </Container>
  );
}

export default App;
